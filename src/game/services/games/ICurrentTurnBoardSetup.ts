import { IChessPiece } from '../pieces/IChessPiece';
import { IPosition } from '../boards/IPosition';
import { IBoard } from '../boards/IBoard';

export interface ICurrentTurnBoardSetup {
    getPieceOnPosition(position: IPosition): IChessPiece;
}

export class CurrentTurnBoardSetup {
    constructor(private _board: IBoard) {}

    public getPieceOnPosition(position: IPosition): IChessPiece {
        const pieceOnBoard = this._board.getPiece(position);
        return pieceOnBoard != null ? pieceOnBoard.piece : null;
    }
}