import { IBoard, Board } from "../boards/IBoard";
import { IChessGame, ChessGame } from "./IChessGame";
import { IPlayer, Player } from "./IPlayer";
import { ChessPieceColor } from "../pieces/ChessPieceColor";
import { IUser } from "../users/IUser";

export interface IChessGameBuilder {
    addPlayer(user: IUser, color?: ChessPieceColor): IChessGameBuilder;
    build(): IChessGame;
}

export class ChessGameBuilder implements IChessGameBuilder {
    private _whitePlayer: IPlayer;
    private _blackPlayer: IPlayer;

    public addPlayer(user: IUser, color?: ChessPieceColor): IChessGameBuilder {
        if (user == null) throw `User can not be null.`;

        const playerColor = color == null 
            ? this._whitePlayer == null ? ChessPieceColor.White : ChessPieceColor.Black
            : color;

        const player = new Player(playerColor, user);

        if (color === ChessPieceColor.White) {
            if (this._whitePlayer != null) {
                throw `White player already added.`
            }
            
            this._whitePlayer = player;
        }

        if (color === ChessPieceColor.Black) {
            if (this._blackPlayer != null) {
                throw `Black player already added.`
            }
            
            this._blackPlayer = player;
        }

        return this;
    }

    public build(): IChessGame {
        const board = new Board();
        board.setUp();

        const chessGame = new ChessGame(board, [this._whitePlayer, this._blackPlayer]);
        
        return chessGame;
    }
}