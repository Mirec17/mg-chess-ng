import { IUser } from '../users/IUser';
import { ChessPieceColor } from '../pieces/ChessPieceColor';

export interface IPlayer {
    color: ChessPieceColor;
    user: IUser;
}

export class Player implements IPlayer {
    public get color(): ChessPieceColor { return this._color };
    public get user(): IUser { return this._user };

    constructor(private _color: ChessPieceColor, private _user: IUser) {}
}