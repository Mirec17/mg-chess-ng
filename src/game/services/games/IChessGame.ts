import { IBoard } from "../boards/IBoard";
import { IPlayer } from "./IPlayer";

import { ChessPieceColor } from '../pieces/ChessPieceColor';
import { IPosition } from "../boards/IPosition";
import { IChessPiece } from '../pieces/IChessPiece';
import { ICurrentTurnBoardSetup, CurrentTurnBoardSetup } from '../games/ICurrentTurnBoardSetup';

export interface IChessGame {
    currentTurnColor: ChessPieceColor;
    currentTurnBoardSetup: ICurrentTurnBoardSetup;

    executeTurn(fromPosition: IPosition, toPosition: IPosition): void;
}

export class ChessGame implements IChessGame {
    private _nextTurnColor: ChessPieceColor;
    public get currentTurnColor(): ChessPieceColor { return this._nextTurnColor; }

    public get currentTurnBoardSetup(): ICurrentTurnBoardSetup { return new CurrentTurnBoardSetup(this._board); }

    constructor(private _board: IBoard, private _players: IPlayer[]) {
        this._nextTurnColor = ChessPieceColor.White;
    }

    public executeTurn(fromPosition: IPosition, toPosition: IPosition): void {
        if (!this._board.isEmpty(toPosition)) {
            this._board.deletePiece(toPosition);
        }

        this._board.movePiece(fromPosition, toPosition);

        this._nextTurnColor = this._nextTurnColor === ChessPieceColor.Black ? ChessPieceColor.White : ChessPieceColor.Black;
        return;
    }
}