export enum ChessPieceColor {
    NotSpecified,
    White,
    Black
}
