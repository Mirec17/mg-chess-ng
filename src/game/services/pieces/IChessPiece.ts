import { ChessPieceKind } from "./ChessPieceKind";
import { ChessPieceColor } from "./ChessPieceColor";
import { IBoard } from "../boards/IBoard";
import { IPosition } from "../boards/IPosition";
import { IChessPieceOnBoard } from "../boards/IChessPieceOnBoard";

export interface IChessPiece {    
    kind: ChessPieceKind;
    color: ChessPieceColor;

    putOnBoard(board: IBoard, position: IPosition): IChessPieceOnBoard;
}

export class ChessPiece implements IChessPiece {
    public get kind() : ChessPieceKind {
        return this._kind;
    }
    
    public get color() : ChessPieceColor {
        return this._color;
    }

    constructor(private _color: ChessPieceColor, private _kind: ChessPieceKind) {};

    putOnBoard(board: IBoard, position: IPosition): IChessPieceOnBoard {
        return board.addPiece(position, this);
    }
}