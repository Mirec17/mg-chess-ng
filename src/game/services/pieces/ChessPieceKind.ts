
export enum ChessPieceKind {
    NotSpecified,
    Pawn,
    Bishop,
    Knight,
    Rook,
    Queen,
    King
}
