import { IPlayer } from "../games/IPlayer";

import { IChessGameBuilder } from '../games/IChessGameBuilder';

export interface IUser {
    name: string;
}