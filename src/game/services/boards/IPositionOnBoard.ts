import { IPosition } from "./IPosition";
import { IBoard } from "./IBoard";

export interface IPositionOnBoard {
    board: IBoard;
    position: IPosition;
}

export class PositionOnBoard implements IPositionOnBoard {
    public get board() : IBoard {
        return this._board;
    }
    
    public get position() : IPosition {
        return this._position
    }

    constructor(private _board: IBoard, private _position: IPosition) {}
}