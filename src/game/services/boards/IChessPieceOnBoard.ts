import { IPositionOnBoard } from "./IPositionOnBoard";
import { IChessPiece } from "../pieces/IChessPiece";
import { IPosition } from "./IPosition";

export interface IChessPieceOnBoard {
    piece: IChessPiece;
    positionOnBoard: IPositionOnBoard;

    move(newPosition: IPosition): IChessPieceOnBoard;
}

export class ChessPieceOnBoard implements IChessPieceOnBoard{
    public get piece(): IChessPiece {
        return this._piece;
    }
    
    public get positionOnBoard() : IPositionOnBoard {
        return this._positionOnBoard;
    }   

    constructor(private _piece: IChessPiece, private _positionOnBoard: IPositionOnBoard) {}

    public move(newPosition: IPosition): IChessPieceOnBoard {
        const board = this.positionOnBoard.board;

        return board.movePiece(this.positionOnBoard.position, newPosition);
    }
}