export interface IPosition {
    row: number;
    column: number;

    equals(position: IPosition): boolean;
}

export class Position implements IPosition {
    public get row(): number {
        return this._row;
    }

    public get column() : number {
        return this._column;
    }

    public equals(position: IPosition): boolean {
        if (this == null || position == null) {
            return false;
        }

        return this.row === position.row && this.column === position.column;
    }

    public toString(): string {
        return `Position: Row: ${this.row} Column: ${this.column}`;
    }
    
    constructor(private _row: number, private _column: number) { }
}