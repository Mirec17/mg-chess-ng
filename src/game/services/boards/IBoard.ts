import { IPosition, Position } from "./IPosition";
import { PositionOnBoard } from "./IPositionOnBoard";
import { IChessPiece, ChessPiece } from "../pieces/IChessPiece";
import { IChessPieceOnBoard, ChessPieceOnBoard } from "./IChessPieceOnBoard";
import { ChessPieceColor } from '../pieces/ChessPieceColor';
import { ChessPieceKind } from '../pieces/ChessPieceKind';

export interface IBoard {
    setUp(): IBoard;
    
    isEmpty(position: IPosition): boolean;
    isValid(position: IPosition): boolean;

    addPiece(position: IPosition, chessPiece: IChessPiece): IChessPieceOnBoard;
    deletePiece(position: IPosition): IChessPiece;
    getPiece(position: IPosition): IChessPieceOnBoard;
    movePiece(position: IPosition, newPosition: IPosition): IChessPieceOnBoard;
}

export class Board implements IBoard {
    private _piecesOnBoard: IChessPieceOnBoard[];

    constructor() {
        this._piecesOnBoard = [];
    }

    setUp(): IBoard {
        [ChessPieceColor.Black, ChessPieceColor.White].map(color => {
            const frontRow = color === ChessPieceColor.Black ? 1 : 6;
            const backRow = color === ChessPieceColor.Black ? 0 : 7;

            let nextColumn = color === ChessPieceColor.Black ? 0 : 7;
            const nextColumnSelector = color === ChessPieceColor.Black
                ? () => nextColumn++
                : () => nextColumn--;

            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Rook));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Knight));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Bishop));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.King));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Queen));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Bishop));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Knight));
            this.addPiece(new Position(backRow, nextColumnSelector()), new ChessPiece(color, ChessPieceKind.Rook));

            for(let i = 0; i <= 7; ++i) {
                this.addPiece(new Position(frontRow, i), new ChessPiece(color, ChessPieceKind.Pawn));
            }
        });
        
        return this;
    }

    isEmpty(position: IPosition): boolean {
        return this.getPiece(position) == null;
    }

    isValid(position: IPosition): boolean {
        return position != null 
            && position.row >= 0 && position.row <= 7
            && position.column >= 0 && position.column <= 7;
    }

    addPiece(position: IPosition, chessPiece: IChessPiece): IChessPieceOnBoard {
        if (!this.isValid(position)) { throw `Invalid postion: ${position}`; };
        if (!this.isEmpty(position)) { throw `Not Empty postion: ${position}`; };
        if (chessPiece == null) { throw `Null chess piece`; };

        const boardPosition = new PositionOnBoard(this, position);
        const pieceOnBoard = new ChessPieceOnBoard(chessPiece, boardPosition);

        this._piecesOnBoard.push(pieceOnBoard);

        return pieceOnBoard;
    }

    deletePiece(position: IPosition): IChessPiece {
        if (!this.isValid(position)) { throw `Invalid postion: ${position}`; };
        if (this.isEmpty(position)) { throw `Empty postion: ${position}`; };

        const pieceToDelete = this.getPiece(position);
        this._piecesOnBoard = this._piecesOnBoard.filter(x => x !== pieceToDelete);

        return pieceToDelete.piece;
    }

    getPiece(position: IPosition): IChessPieceOnBoard {
        const pieceOnPosition = this._piecesOnBoard.filter(x => x.positionOnBoard.position.equals(position))[0];
        return pieceOnPosition;
    }

    movePiece(position: IPosition, newPosition: IPosition): IChessPieceOnBoard {
        if (!this.isValid(position)) { throw `Invalid postion: ${position}`; };
        if (!this.isValid(newPosition)) { throw `Invalid new postion: ${position}`; };
        if (this.isEmpty(position)) { throw `Empty postion: ${position}`; };
        if (!this.isEmpty(newPosition)) { throw `Not Empty postion: ${position}`; };

        const deletedPiece = this.deletePiece(position);
        const addedPiece = this.addPiece(newPosition, deletedPiece);

        return addedPiece;
    }
}