import { Component, OnInit } from '@angular/core';
import { IChessGame } from 'src/game/services/games/IChessGame';
import { IChessPiece } from 'src/game/services/pieces/IChessPiece';
import { ChessGameBuilder } from 'src/game/services/games/IChessGameBuilder';
import { Position, IPosition } from 'src/game/services/boards/IPosition';

@Component({
    selector: 'board',
    template: `
        <div class="board bg-dark">
            <ng-container *ngFor='let row of [0, 1, 2, 3, 4, 5, 6 , 7]'>
                <ng-container *ngFor="let column of [0, 1, 2, 3, 4, 5, 6 , 7]">
                    <board-cell
                        [isSelected]="this.isSelected(row, column)"
                        [row]="row"
                        [column]="column"
                        [pieceOnCell]="this.getPieceOnPosition(row, column)"
                        (onClick)="this.changeCellSelection(row, column)">
                    </board-cell>
                </ng-container>
            </ng-container>
        </div>
    `
})
export class BoardComponent implements OnInit {
    private _game: IChessGame;
    public selectedCellPosition: IPosition;

    constructor() {
    }

    ngOnInit(): void {
        this._game = new ChessGameBuilder()
            .addPlayer({ name: 'John' })
            .addPlayer({ name: 'Tom' })
            .build();
    }

    public isSelected(row: number, column: number): boolean {
        return this.selectedCellPosition != null
            && this.selectedCellPosition.row === row
            && this.selectedCellPosition.column === column;
    }

    public getPieceOnPosition(row: number, column: number): IChessPiece {
        var pieceOnBoard = this._game.currentTurnBoardSetup.getPieceOnPosition(new Position(row, column));
        return pieceOnBoard;
    }

    public changeCellSelection(row: number, column: number): void {
        if (this.isSelected(row, column)) {
            this.selectedCellPosition = null;
            return;
        }

        const currentTurnBoardSetup = this._game.currentTurnBoardSetup;

        const newSelectedPosition: IPosition = new Position(row, column);
        const pieceOnNewSelectedPosition = currentTurnBoardSetup.getPieceOnPosition(newSelectedPosition);

        if (this.selectedCellPosition != null) {
            const pieceOnCurrentSelectedPosition = currentTurnBoardSetup.getPieceOnPosition(this.selectedCellPosition);
            //Piece is going to be moved!
            if (pieceOnNewSelectedPosition == null /* Move */
                || pieceOnNewSelectedPosition.color !== this._game.currentTurnColor /* Kill */) {
                //we are going to move the piece
                this._game.executeTurn(this.selectedCellPosition, newSelectedPosition);
                this.selectedCellPosition = null;
                return;
            } else if (pieceOnNewSelectedPosition.color === this._game.currentTurnColor) {
                //select different piece
                this.selectedCellPosition = newSelectedPosition;
                return;
            }
        } else {
            //No piece selected yet
            if (pieceOnNewSelectedPosition == null) {
                return;
            }

            if (pieceOnNewSelectedPosition.color !== this._game.currentTurnColor) {
                return;
            }

            this.selectedCellPosition = newSelectedPosition;
        }
    }
}