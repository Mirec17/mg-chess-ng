import { ChessPieceKind } from './../../services/pieces/ChessPieceKind';
import { IChessPiece } from './../../services/pieces/IChessPiece';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ChessPieceColor } from 'src/game/services/pieces/ChessPieceColor';

@Component({
    selector: 'board-cell',
    template: `
        <div class="board-cell" [ngClass]="this.colorClass">
           <button class="btn btn-block bg-transparent h-100" (click)="this.onCellClicked()">
               <div *ngIf="this.pieceOnCell != null" class="chess-piece-image" [ngClass]="this.getPieceClass(this.pieceOnCell)">
               </div>
           </button>
        </div>
    `
})
export class BoardCellComponent {
    @Input() public row: number;
    @Input() public column: number;
    @Input() public isSelected: boolean;
    @Input() public pieceOnCell: IChessPiece;

    @Output() public onClick: EventEmitter<undefined>;

    public get isWhite(): boolean { return (this.row % 2 === 0) === (this.column % 2 === 0); }

    public get colorClass(): string {
        if (this.isSelected) {
            return 'bg-warning';
        }

        return this.isWhite ? "bg-light" : "bg-success";
    }

    constructor() {
        this.onClick = new EventEmitter<undefined>();
    }

    public onCellClicked(): void {
        this.onClick.emit();
    }

    public getPieceClass(pieceOnCell: IChessPiece): string {
        let pieceType = "";
        switch(pieceOnCell.kind) {
            case(ChessPieceKind.Pawn): { pieceType = "pawn"; break; }
            case(ChessPieceKind.Rook): { pieceType = "rook"; break; }
            case(ChessPieceKind.Knight): { pieceType = "knight"; break; }
            case(ChessPieceKind.Bishop): { pieceType = "bishop"; break; }
            case(ChessPieceKind.Queen): { pieceType = "queen"; break; }
            case(ChessPieceKind.King): { pieceType = "king"; break; }
        }

        let color = pieceOnCell.color === ChessPieceColor.Black ? "black" : "white";

        return `${color}-${pieceType}`;
    }
}