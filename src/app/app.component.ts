import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container">
      <div class="row">
        <div class="col-3">
          <board>
          </board>
        </div>
      </div> 
    </div>
  `
})
export class AppComponent {
  title = 'chess';
}
